# Aligning sequences against the human genome with STAR

## Objectives

## Prerequisites

## Basic Sequence Alignment Principles

## STAR and the human genome reference

build the STAR index using the fasta file in ../../reference/

## Aligning our sequences

how to align sequences and store in BAM format

might need to introduce samtools
