# A few helpful tips before we begin
When refering to commands that we will type into the command line

1. **Text** Naked text not wrapped in any characters are commands. They are usually the names of the programs we are calling. 


2. **<Text>** When Text is wrapped in angular brackets they are positional arguments and are required to run the program.


3. **[Text]** Text wrapped in square brackets are optional. They are extra arguments you can specify depending on your use case


4. **-abcxyz** Any hypen/minus sign preceeding a letter is called an option or flag. [-a option_argument] If an option has a argument that goes with it, it will be grouped with the argument. 


5. **(text|text2)** text wrapped in parenthesis are required but not position specific. When there is a '|' character between two forms of text, either command can be used.


6. **text...** text with a proceeding elipse means that multiple inputs can be taken. This is commonly used when specifying that multiple input files or output files can be created. 


 
All of this information can be read more indepth [here](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html). To Find out more 
as to why historically some options are tied to specific functions you can read more [here](http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_09_04).


# Compiling QC Information with multiqc

[MultiQC](http://multiqc.info/docs/) is an amazing tool with excellent documentation. It is designed to take all of the supported tools and create a monolithic interactive HTML file which displays releveant information like interactive graphs with the ability to hide certain values and sample names

## Objectives

  - learn how to use multiqc

## Prerequisites

Multiqc should be run after all the other analyses are completed as it will build a file that contains all of their results.

## QC of a collection of datasets

To run it is quite simple but a few things must be specified before hand. Since you are writing a Qsub script you still need a shebang and parameters. There is a obscure error that you can get when running multiqc which will say something along the lines of ```ValueError: unknown locale: UTF-8```. This is due to MatPlotLib not liking certain string characters. To fix this you must add  ```export LC_ALL=en_US.utf-8 export LANG=$LC_ALL &&``` before you shell script.

## multiqc

To call multiqc just type ```multiqc``` to specify a custom file name you must use the options ```(-n|--name)```. Multiqc can take any number of inputs which are just source files generated from the other analyses. Just typing ```multiqc .``` runs multiqc on the current working directory. Multiqc intelligently finds the files it can read. 

**Note**

- Multiqc doesn't take the paired and unpaired trim files from Trimmomatic. Rather it reads the information that was redirected from standard out into a text file. 


## Your third qsub script: multiqc

```
#!/bin/bash

#$project specification and other arguments

#shell script

<export LC_ALL=en_US.utf-8> <export LANG=$LC_ALL> <&&> <multiqc> (-n|--name) <inputs>... 

```

