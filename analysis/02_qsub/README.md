# 02 - Bash scripts and qsub

###  Bash scripts

A Bash script is a plain text file which contains a series of commands. These commands are a mixture of commands we would normally type ouselves on the command line (such as ls or cp for example) and commands we could type on the command line but generally would not. An important point to remember though is:

**Anything you can run normally on the command line can be put into a script and it will do exactly the same thing. Similarly, anything you can put into a script can also be run normally on the command line and it will do exactly the same thing.**

You don't need to change anything. Just type the commands as you would normally and they will behave as they would normally. It's just that instead of typing them at the command line we are now entering them into a plain text file. In this sense, if you know how to do stuff at the command line then you already know a fair bit in terms of Bash scripting. It is convention to give files that are Bash scripts an extension of .sh (myscript.sh for example).

### Qsubs

**What is qsub?**
qsub is the command used for job submission to the cluster. It takes several command line arguments and can also use special directives found in the submission scripts or command file.

## Objectives

To make running pipeline simpler. Instead of writing code directly on the terminal, you could write them on a script and run it. For multiple jobs, it is advisable to use qsubs to prevent getting reaped.

## Prerequisites
- Shebang line at start of script
- '$' for variables and #$ infront of options

## Bash scripting basics

**Always start a bash script with:**
```
#!/bin/bash
```
This line is also known as the shebang (usually written as first line of the script). 

### Example simple scripts
```
#!/bin/bash
# A sample bash

echo Hello World
```
The first line indicates the system which program to use to run the file.
The second line is a comment and anything after the # is not executed.
The third line is the only action performed by this script, which prints 'Hello World' on the terminal.

**Bash scripts work like python scripts. You can also do:**
```
#!/bin/bash
STR="Hello World!"
echo $STR
```
Here, we created a variable called STR and assign the string "Hello World!" to it. Then the value of this variable is retrieved by putting the '$' in at the beginning. If you don't use the '$' sign, the output of the program will be different and probably not what you want it to be.

**For loops:**
```
#!/bin/bash
for i in $(ls); do
        echo item: $i
done
```
**To make file executable:**
```
chmod +x myscript.sh
```
**To run bash scripts:**
```
./myscript.sh
```
where myscript could be any name of the script.

**For more information, see:**
http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html#toc10
https://linuxconfig.org/bash-scripting-tutorial

### Qsub basics

For qsub information or help:
```
man qsub
qsub -help
```

**To run qsub:**
```
qsub [ options ] [ command | -- [ command_args ]]
```

**Example:**
```
qsub myscript.qsub
```

**List of some options:**
```
   [-clear]                                 skip previous definitions for job
   [-cwd]                                   use current working directory
   [-help]                                  print this help
   [-o path_list]                           specify standard output stream path(s)
   [-P project_name]                        set job's project
   [-pe pe-name slot_range]                 request slot range for parallel jobs
```
For more options:
```
qsub -help
```
For more detailed description of options: http://scv.bu.edu/scc_manpages/qsub.txt

## Your first bash script: fastqc
** Example qsub for fastqc:**
```
#!/bin/bash
#$ -P mlhd
#$ -cwd

R1=/home/sampleR1.fastq.gz
R2=/home/sampleR2.fastq.gz

fastqc $R1 $R2
```

## Submitting jobs scripts with qsub
**To submit qsub job :**
```
qsub [ options ] [ command | -- [ command_args ]]
```
**Example:**
```
qsub myscript.qsub
```
