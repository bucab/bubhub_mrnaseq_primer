# 00 - Preliminaries and terminal_quest

## Objectives

  - Introduce core concepts and prerequisites for these tutorials
  - Set up command line environment needed to complete this primer
  - Install terminal_quest using conda and use it to familiarize yourself
    with basic command line usage

## Prerequisites

This tutorial assumes you have already followed the setup instructions in the
main README of this repo (namely, installed [conda](https://conda.io), created
an environment, and installed the packages using the
`install_conda_packages.sh` script.

## Introduction

This tutorial is the first in a set of materials intended to teach practical
use of computational tools commonly employed in bioinformatics. It uses an
mRNA-Seq analysis pipeline as the vehicle for this content. By the end of the
tutorial series, you will have gained the skills:

  - basic command line usage
  - how to run commands on the command line to perform elimentary
    bioinformatic analysis
  - how to write bash scripts to perform this analysis and submit them to run
    on a linux cluster
  - how to write expressive, replicable workflows with `snakemake`
  - rudimentary assessment of quality control for high throughput sequencing
    data

## The Data

In the `samples/` director of this repo are samples with real mRNA-Seq
data. There are two subdirectories:

  - `samples/small`: data files with 100k sequences each
  - `samples/large`: data files with 1M sequences each

The data files are in [gzipped](www.gzip.org)
[fastq](https://en.wikipedia.org/wiki/FASTQ_format). A fastq file contains
sequences generated from high-throughput sequencing instruments such as the
[Illumina HiSeq](https://www.illumina.com/systems/sequencing-platforms/hiseq-3000-4000.html)
series of sequencers. We will be analyzing these samples later in this
tutorial series.

## Linux as a computational platform

The linux operating system is a free and powerful tool very commonly used
in computational analysis. Many bioinformatic tools are developed and designed
to run on linux, and large-scale linux clusters enable efficient and timely
analysis of very large datasets.

Linux clusters are typically accessed using a *command line interface (CLI)*.
In a CLI, text commands are typed into a *prompt* followed by pressing the
Enter key, which executes the given command. The prompt is the string of text
at the beginning of a line in the CLI preceding text input. The format of the
prompt varies based on the linux environment you are using, but for the
purposes of this tutorial the string `$> ` indicates the prompt in all the
following examples.

There are thousands of commands available on the command line, but only a
handful are needed to perform most important operations in bioinformatic
analyses.  Commands do essentially three basic things:

  - read from files
  - write to new or existing files
  - print information to the screen for you to view

A given command may do any or all of these these three basic operations. For
example, the `ls` command only prints information about the files in the
current directory to the screen (executed in the current directory):

```
$> ls
README.md
$>
```

In the current directory, i.e. `00_terminal_quest`, there is only a single file,
named `README.md` (which you are currently reading). After the command printed
the file listing to the screen, the command ended and returns control to a new
prompt.

In the previous example, `ls` is called an `executable` or `program`. Many
executables can also accept *arguments*, passed as additional text separated by
spaces. For example:

```
$> ls ../../
analysis  conda_packages.txt  install_conda_packages.sh  README.md  reference  samples
$>
```

Here, the `ls` command is given an argument, in this case `../../`. In linux,
the string `..` represents the parent directory to the current directory. The
parent directory of the `00_terminal_quest` directory is `analysis`, and the
parent directory of `analysis` is the root of the repository. When directories
are listed together, they are separated by the `/` symbol. Thus, passing
`../../` to the `ls` command lists the files in the parent directory of the
parent directory of the current directory.

The above examples illustrate most of the fundamental principles of using a
CLI. Only the executables and the appropriate arguments change.

For a gentle but more thorough introduction to command line use, I
highly recommend [this tutorial series](http://linuxcommand.org/).

## Terminal Quest

[terminal_quest](https://bitbucket.org/bubioinformaticshub/terminal_quest)
is a software that creates a series of puzzles that can be solved with only
the following CLI commands:

  - `cd` - [change the current directory](http://linuxcommand.org/lc3_lts0020.php)
  - `cat` - print a file or files to the screen
  - `head` - print just the first lines of a file or files to the screen
  - `tail` - print just the last lines of a file or files to the screen
  - `ls` - [list the file names or file information](http://linuxcommand.org/lc3_lts0030.php)
  - `man` - [display usage information about a command](http://linuxcommand.org/lc3_lts0060.php) (e.g. `man ls`)
  - `grep` - [search for the text in a file](https://www.panix.com/~elflord/unix/grep.html)
  - [the glob wildcard character `*`](http://linuxcommand.org/lc3_lts0050.php) (e.g. `ls *.txt`),
  - [the redirection character `>`](http://linuxcommand.org/lc3_adv_redirection.php) (e.g. `cat A.txt B.txt > A_and_B.txt`)

If you installed the conda packages for this repo successfully, you
can create your own terminal quest by running the following in any
directory:

```
$> terminal_quest
```

Instructions will be printed to the screen, and the directory `level0`
will be created in the current directory. To get started, run:

```
$> cd level0
$> cat level0.md
```

There are 10 levels in all (the last level is `level9`). As a hint, in
the last level, sunglasses are involved.

Once you are comfortable with the commands covered in this tutorial,
you are ready to move onto `01_fastqc`.
