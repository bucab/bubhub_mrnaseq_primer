# A few helpful tips before we begin

When refering to commands that we will type into the command line

1. **Text** Naked text not wrapped in any characters are commands. They are usually the names of the programs we are calling.


2. **<Text>** When Text is wrapped in angular brackets they are positional arguments and are required to run the program.


3. **[Text]** Text wrapped in square brackets are optional. They are extra arguments you can specify depending on your use case


4. **-abcxyz** Any hypen/minus sign preceeding a letter is called an option or flag. [-a option_argument] If an option has a argument that goes with it, it will be grouped with the argument.


5. **(text|text2)** text wrapped in parenthesis are required but not position specific. When there is a '|' character between two forms of text, either command can be used.


6. **text...** text with a proceeding elipse means that multiple inputs can be taken. This is commonly used when specifying that multiple input files or output files can be created.

All of this information can be read more indepth [here](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html). To Find out more
as to why historically some options are tied to specific functions you can read more [here](http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_09_04).

# Improving sequence quality with trimmomatic

Trimmomatic improves sequence quality by trimming off the adapters that were used during sequencing and are no longer necessary after the file has been generated.

## Objectives
  - Introduce some posix commands
  - Introduce Trimmomatic
  - Learn how to write a qsub script

## Prerequisites

This tutorial assumes you have already followed the setup instructions in the main README of this repo (namely, installed [conda](https://conda.io), created an environment, and installed the packages using the `install_conda_packages.sh` script. Also that you are following the steps in order. Other than this, Trimmomatic requires no files generated from any of the other previous analyses.

## Sequence quality and PHRED scores

PHRED scores is a parameter that you can specify in Trimmomatic. There are two Phred scores that can be used in Trimmomatic, Phred33 and Phred64, but there are others. Phred scores allow you to assess the quality of your sequence and are denoted as ascii characters in the FastQ file. It determines how confident you are that the base that is shown, is actually the base that was sequenced. To denote phred score you use the option. More information about phred scores can be found [here](https://en.wikipedia.org/wiki/FASTQ_format#Encoding).


## Sequencing adapters

When sequencing, short olignucletide DNA adapters of known sequence are used. They help the sequencer identify and read the sequences during sequencing. Their are a variety of Illumina adapters (Nextera, TruSeq, and TruSight), but we use TruSeq adapters in our analyses. For Paired end sequences they will be located in a file called TruSeq\#_PE.fa. For single end the file structure is similarm just replace PE with SE.

## trimmomatic basics

[trimmomatic](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf) is a command line tool that can be used to trim Illumina(FASTQ) data. I will break down the command into multiple parts. The first part is the command to invoke Trimmomatic itself, which is just ``trimmomatic``.


Trimmomatic workes in two modes. Single end, and Paired end. If the data is Single end, you will only have reads for the forward strand. In Paired end you will have reads for both the forward and reverse strand. If you are working with data that contains 'sample1\_R1' and 'sample1\_R2' It's probably Paired end. If so use ``PE`` if not use ``SE`` to denote the type of data it will encounter.


The next step is to denote what PHRED score the data was calculated with. There are two Phred scores that can be used Phred33 and Phred64. ``-phred#`` with \# being the score that was used on your data.

Trimmomatic requires inputs and outputs in the form of ```input1 input2 output-paired1 output-unpaired1 output-paired2 output-unpaired2```. For Paired end mode. For single end mode there will only be one output per sample. The input is the source file you are trying to trim. The output files are the name of the files you want to create. It is very important to keep the order of the outputs correct in Paired end mode. In paired end mode,  the first output is the paired output. A paired output is created when  both the forward and reverse read survive the trimming process. By contrast the  unpaired output contains contains only the single read that survived.

Next you want to show trimmomatic the sequence of the adaptor so it knows what to trim. The command is ```ILLUMINACLIP:/path/to/.fafile:2:30:10``` This is usually given in some sort of '.fa' file. For example ```TruSeq3-PE.fa```, ```2:30:10``` are defaults for the trimming and you probably will never have to touch them.

The last part of the command allows you to fine tune the trimming. It is usually better to leave these at their default setting. They are specified with, LEADING, TRAILING, SLIDINGWINDOW, and MINLEN. You can read more about them in the Trimmomatic Documentation. The default command is ```LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36```.


Lastly some other programs need the the output that Trimmomatic creates as it is running. You must capture it using a redirect. The command is ```2>Textfile.txt``` to redirect the stdout to a text file. If the text file doesn't exist it will be created. This needs to be done for every sample you have.


## Your second qsub script: trimmomatic

A qsub script allows you to wrap a bash command that you would normally run in the terminal, into an executeable script. This makes it easy to submit a job to the cluster.  Qsub scripts usually end in  ``.sh``. They are run by typing ```./some\_qsub\_script.sh``` on the command line. If it is not executable be sure to make it with ```chmod```. Qsub scripts start with a Shebang ``!#bin/bash`` which just tells the terminal to use the bash interpretor.


A Qsub script can have a number of options set to it. The option parameters are preceeded by ```#$```. Every Qsub script will have a project associated with it. Use the ```#$ -P option_argument``` option to denote the project associated with the job, and add the name of your project as the option argument.


I recommend you run the job in the current working directory unless you have a reason not to. This can be denoted with ```#$-cwd``` parameter.


Comments in bash scripts are denoted with a `#` symbol. Variables are created using the equals sign, ```foo=bar``` with no space between the variable name and it's value. To call the contents of a variable you must add a ```$``` before the variable. It is good practice to wrap the names of your input and output files in variable names.


IE
```
input1=/path/to/sample1
input2=/path/to/sample2

output1=/path/to/output1
output2=/path/to/output2

trimmomatic $input1 $input2 $output1 $output2
```


This can make your script more readable when you are calling files with long names and paths.

**Note**


- If the files are in The same directory as the script, you can just use their names. If not you have to specify the path to the file

- If you want the output to be in the same directory as the script, then don't give a path to the output preceeding the name of the ouputfile:

IE

/user/samples/output1

vs

output1

### Qsub recap

file structure looks like this

```
#!/bin/bash

#$ options

variables

shell command

```

# Putting all of this together


```
#!/bin/bash

#options
#$(-P option_argument)
#$(-cwd)

#shell command
trimmomatic (PE|SE) (-phred33|-phred64) <input>... (<output-paired> <output-unpaired>)... (ILLUMINACLIP:/path/to/adaptors:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36) [2>somefile.txt]

```
