conda install -c bubhub -c bioconda -c conda-forge python=3.5 fastqc \
  trimmomatic snakemake star samtools rseqc git-lfs terminal-quest
conda list > conda_packages.txt
