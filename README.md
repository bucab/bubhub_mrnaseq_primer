# README #

This repository contains materials and instruction to learn skills necessary for implementing an mRNA-Seq pipeline on a linux cluster. This includes the following:

  1. Basic command line usage via [terminal-quest](https://bitbucket.org/bubioinformaticshub/terminal_quest)
  1. Running FastQC on small mRNA-Seq datasets
  1. Writing bash scripts and submitting them via qsub
  1. Running trimmomatic on the included mRNA-Seq datasets and assessing sequence quality improvement
  1. Compiling a full QC report using [multiqc](http://multiqc.info)
  1. Reimplementing all above analysis steps into a [snakemake](https://snakemake.readthedocs.io/en/stable/)
  1. Aligning the reads with STAR against a reduced human reference genome
  1. Assessing the quality of the alignments with RseQC
  1. Performing differential expression analysis (DE) with [detk](https://bitbucket.org/bubioinformaticshub/de_toolkit)

### Prerequisites and Setup ###

This primer assumes you are comfortable connecting to a linux cluster using
`ssh` and that you have [conda](https://conda.io) installed in your cluster
environment. To get started, first fork this repository in bitbucket by
clicking on the `+` on the left side of the screen and selecting `Fork this
repository`. This will create a repository in your own bitbucket account, which
you should clone to your linux cluster environment:

```
git clone git@bitbucket.org:<your username>/bubhub_mrnaseq_primer.git
```

Make sure to replace `<your username>` appropriately for your account. Once
your repo is cloned, create a conda environment named `bubhub_mrnaseq_primer`
and activate it:

```
conda create -n bubhub_mrnaseq_primer python=3.5
source activate bubhub_mrnaseq_primer
```
Once your environment is created, make sure your current working directory is
the repo root and install the conda dependencies for this primer:

```
./install_conda_packages.sh
```

After the packages are installed, you may begin playing using the materials
under `analysis/`.
